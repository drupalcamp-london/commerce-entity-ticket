#Commerce Entity Ticket
Allows user to purchase a ticket as a product type and get assigned the ticket.

Upon purchase of ticket a new user role will be assigned that shows the user has access to the ticket.
The ticket will be assigned to purchasee and then from this they will be the "owner" of the ticket(s) purchased. 
Each ticket can be assigned to a user.


