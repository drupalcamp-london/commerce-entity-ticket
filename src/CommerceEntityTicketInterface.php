<?php

namespace Drupal\commerce_entity_ticket;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Ticket entity.
 */
interface CommerceEntityTicketInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}