<?php
/**
 * Created by PhpStorm.
 * User: alexburrows
 * Date: 12/09/2018
 * Time: 16:14
 */

namespace Drupal\commerce_entity_ticket;


use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

class Ticket extends ContentEntityBase implements CommerceEntityTicketInterface {

  use EntityChangedTrait; // Implements methods defined by EntityChangedInterface.

  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
  }


}